<?php
/**
 * @file
 * rules hooks
 */


/**
 * Implements hook_rules_event_info().
 *
 * This function declares all the possible nodejs.
 * @todo
 */
function nodejs_rules_rules_event_info() {
  // @todo: here we could provide an entry point for events coming in from nodejs?
  return array();
}

/**
 * Implements hook_rules_event_info().
 *
 * This function declares all nodejs_rules actions
 */
function nodejs_rules_rules_action_info() {
  // we're only implementing actions for the node entity-type
  return array(
    'nodejs_rules_notify_broadcast' => array(
      'label' => t('nodejs: Broadcast a message to everyone'),
      'parameter' => array(
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject to broadcast'),
          'optional' => FALSE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Text to broadcast'),
          'optional' => FALSE,
        ),
      ),
      'group' => t('nodejs_rules_notify'),
      'base' => 'nodejs_rules_notify_broadcast',
      'callbacks' => array(),
    ),
    'nodejs_rules_notify_user' => array(
      'label' => t('nodejs: Send a message to a user'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User to notify')
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject to send'),
          'optional' => FALSE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Text to send'),
          'optional' => FALSE,
        ),
      ),
      'group' => t('nodejs_rules_notify'),
      'base' => 'nodejs_rules_notify_user',
      'callbacks' => array(),
    ),
    /*
    'nodejs_rules_notify_token_channel' => array(
      'label' => t('nodejs: Send a notification to a tokenChannel'),
      'parameter' => array(
        'channel' => array(
          'type' => 'text',
          'label' => t('Channel'),
          'optional' => FALSE,
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject to send'),
          'optional' => FALSE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Text to send'),
          'optional' => FALSE,
        ),
      ),
      'group' => t('nodejs_rules_notify'),
      'base' => 'nodejs_rules_notify_token_channel',
      'callbacks' => array(),
    ),
    'nodejs_rules_add_user_to_channel' => array(
      'label' => t('nodejs: Subscribe a user to a channel'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User to subscribe')
        ),
        'channel' => array(
          'type' => 'text',
          'label' => t('Channel'),
          'optional' => FALSE,
        ),
      ),
      'group' => t('nodejs_rules'),
      'base' => 'nodejs_rules_add_user_to_channel',
      'callbacks' => array(),
    ),
    'nodejs_rules_remove_user_from_channel' => array(
      'label' => t('nodejs: Remove user from channel'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User to subscribe')
        ),
        'channel' => array(
          'type' => 'text',
          'label' => t('Channel'),
          'optional' => FALSE,
        ),
      ),
      'group' => t('nodejs_rules'),
      'base' => 'nodejs_rules_remove_user_from_channel',
      'callbacks' => array(),
    ),*/
  );
}

function nodejs_rules_notify_broadcast($subject, $message,  $element=NULL) {
  nodejs_broadcast_message($subject, $message);
}

function nodejs_rules_notify_user($user, $subject, $message,  $element=NULL) {
  nodejs_send_user_message( $user->uid, $subject, $message);
}

function nodejs_rules_notify_token_channel($channel, $subject, $message,  $element=NULL) {
//  drupal_set_message( t('nodejs_rules_notify_token_channel to channel @channel: @subject | @message', array('@channel' => $channel, '@message' => $message,  '@subject' => $subject)) );
  $message = (object) array(
    'channel' => $channel,
    'broadcast' => FALSE,
    'data' => (object) array(
      'subject' => $subject,
      'body' => $message,
    ),
    'callback' => 'nodejsNotify',
  );
  nodejs_send_content_channel_message($message);
}

function nodejs_rules_add_user_to_channel($user, $channel) {
  drupal_set_message( t('nodejs_rules_add_user_to_channel user @user_name to channel @channel', array('@user_name' => $user->name, '@channel' => $channel)) );
  nodejs_add_user_to_channel($user->uid, $channel);
}

function nodejs_rules_remove_user_from_channel($user, $channel) {
  drupal_set_message( t('nodejs_rules_remove_user_from_channel user @user_name to channel @channel', array('@user_name' => $user->name, '@channel' => $channel)) );
  nodejs_remove_user_from_channel($user->uid, $channel);
}